import sqlite3
#############Selection of ID, Name and Salary in EMPLOYEE Table##############
con = sqlite3.connect('department.db')
cursor1 = con.cursor()
cursor1.execute('select e_id,e_name,salary from EMPLOYEE')
headings = [f"{id} \t\t{name} \t\t{sal}" for id,name,sal in cursor1.fetchall() ]
id,name,sal = 'Employee ID','Employee Name','Salary'
print('\n'.join([f"{id}\t{name}\t  {sal}"] + headings))
con.commit()
con.close()

###########################################


