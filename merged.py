import sqlite3

#############Creating EMPLOYEE Table##############

conn = sqlite3.connect('department.db')
# cursor object
cursor = conn.cursor()
# drop query
cursor.execute('DROP TABLE IF EXISTS EMPLOYEE')
# create query
query = """CREATE TABLE EMPLOYEE(e_id INT PRIMARY KEY NOT NULL, e_name text,salary INT,department_id INT, FOREIGN KEY(department_id) REFERENCES DEPARTMENTS(department_id) )"""
cursor.execute(query)
conn.commit()
#conn.close()

###########################################


#############Creating City Column in EMPLOYEE Table##############
#alter_conn = sqlite3.connect('department.db')
# cursor object
#alter_cursor = alter_conn.cursor()
# alter table query
alter_query = """ALTER TABLE EMPLOYEE ADD COLUMN city text"""
cursor.execute(alter_query)
conn.commit()
#alter_conn.close()

###########################################

#############Insertion in EMPLOYEE Table##############
#insert_conn = sqlite3.connect('department.db')
conn.execute("INSERT INTO EMPLOYEE (e_id,e_name,salary,department_id,city) "
             "VALUES (1, 'John', '70000',101,'Bangalore')")
conn.execute("INSERT INTO EMPLOYEE (e_id,e_name,salary,department_id,city) "
             "VALUES (2, 'Sara', '50000',102,'Nagpur')")
conn.execute("INSERT INTO EMPLOYEE (e_id,e_name,salary,department_id,city) "
             "VALUES (3, 'Mary', '45000',103,'Vijaywada')")
conn.execute("INSERT INTO EMPLOYEE (e_id,e_name,salary,department_id,city) "
             "VALUES (4, 'Peter', '50000',104,'Cochin')")
conn.execute("INSERT INTO EMPLOYEE (e_id,e_name,salary,department_id,city) "
             "VALUES (5, 'Ryan', '40000',105,'Trivandrum')")

###########################################



#d_cursor = d_conn.cursor()
# drop query
cursor.execute('DROP TABLE IF EXISTS DEPARTMENTS')
# create query
d_query = """CREATE TABLE DEPARTMENTS(department_id INT PRIMARY KEY NOT NULL, department_name text)"""
cursor.execute(d_query)
conn.commit()
#conn.close()

###########################################

#############Insertion in EMPLOYEE Table##############

conn.execute("INSERT INTO DEPARTMENTS(department_id,department_name) VALUES (101,'Board of Governors');")
conn.execute("INSERT INTO DEPARTMENTS(department_id,department_name) VALUES (102,'Operations');")
conn.execute("INSERT INTO DEPARTMENTS(department_id,department_name) VALUES (103,'Marketing');")
conn.execute("INSERT INTO DEPARTMENTS(department_id,department_name) VALUES (104,'Finance');")
conn.execute("INSERT INTO DEPARTMENTS(department_id,department_name) VALUES (105,'Technology');")
conn.commit()
conn.close()

###########################################

read_con1 = sqlite3.connect('department.db')
read_cursor1 = read_con1.cursor()
n = int(input('Enter the Department ID: '))
read_cursor1.execute(f"SELECT EMPLOYEE.e_id, EMPLOYEE.e_name, EMPLOYEE.salary,EMPLOYEE.city,DEPARTMENTS.department_name FROM EMPLOYEE,DEPARTMENTS ON EMPLOYEE.department_id = DEPARTMENTS.department_id WHERE EMPLOYEE.department_id = {n}")
print(read_cursor1.fetchall())
read_con1.commit()
read_con1.close()
