import sqlite3
e_read_conn = sqlite3.connect('department.db')
e_read_cursor = e_read_conn.cursor()
user_response = 1
while user_response != 0:
    id = input('Enter the Employee ID for which you want to Update Employee Name: ')
    naam = input('Enter the Name to which the Employee Name to be Updated: ')
    update_query = 'update EMPLOYEE set e_name = ? where e_id = ?;'
    ip_tuple = (naam,id)
    e_read_cursor.execute(update_query,ip_tuple)
    print('Table Updated Successfully !!!')
    user_response = int(input('Do you want to Continue: (0/1) '))
e_read_conn.commit()
e_read_conn.close()